" Vundle
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Plugins
Plugin 'preservim/nerdtree'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
" Plugin 'altercation/vim-colors-solarized'
Plugin 'doums/darcula'

call vundle#end()
filetype plugin indent on

" End Vundle

let mapleader = "\<Space>"

syntax on
set relativenumber
set pastetoggle=<F2>

set background=dark
colorscheme darcula

" use w!! to save as sudo
cmap w!! w !sudo tee % >/dev/null

" NERDTree config
map <C-n> :NERDTreeToggle<CR>

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='base16_gruvbox_dark_hard'

" No backups
set nobackup
set noswapfile

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Easy switch buffer
map <leader>l :bn<cr>
map <leader>h :bp<cr>


